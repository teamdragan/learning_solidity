# README #

## What is this repository for? ##

Tutorials from Yutube series Learn Solidity:

1. [The Basics](https://www.youtube.com/watch?v=v_hU0jPtLto)
1. [Inheritance](https://www.youtube.com/watch?v=6hkmLOtIq8A)
1. [Custom Modifiers and Error Handling](https://www.youtube.com/watch?v=3ObTNzDM3wI)
1. [Imports and Libraries](https://www.youtube.com/watch?v=0Lyf_3kA3Ms)
1. [Event Logging and Transaction Information](https://www.youtube.com/watch?v=Jlq997yOoRs)
1. [Data Types (array, mapping, struct)](https://www.youtube.com/watch?v=8UhO3IKApSg)
1. [Extending String Functionality and Bytes]
1. [Debugging Solidity using Remix](https://www.youtube.com/watch?v=7z52hP26MFs&list=PL16WqdAj66SCOdL6XIFbke-XQg2GW_Avg&index=8)
1. [ERC20 Tokens and Creating your own Crypto Currency](https://www.youtube.com/watch?v=r7XojpIDuhA&list=PL16WqdAj66SCOdL6XIFbke-XQg2GW_Avg&index=9)
1. [ERC223 Tokens and Creating your own Crypto Currency](https://www.youtube.com/watch?v=IWC9-yGoDGs&list=PL16WqdAj66SCOdL6XIFbke-XQg2GW_Avg&index=10)

## How do I get set up? ##

### Remix IDE (http://remix.ethereum.org)

* No special setup required, just go to the website

1. Open a contract in Remix
1. Compile (make sure no errors)
1. Run - Create (contract is deployed)
1. Interact with contract

### Truffle (TODO)

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Dragan Nikolic (dragan.d.nikolic@gmail.com)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
