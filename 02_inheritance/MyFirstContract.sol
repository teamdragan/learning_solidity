pragma solidity ^0.4.0;

interface Regulator {
    function checkValue(uint amount) view public returns(bool);    
    function loan() view public returns(bool);
}

contract Bank is Regulator {
    uint private value;

    function Bank(uint amount) public {
        value = amount;        
    }

    function deposit(uint amount) public {
        value += amount;
    }
    
    function withdraw(uint amount) public {
        if (checkValue(amount)) {
            value -= amount;
        }
    }
    
    function balance() view public returns (uint) {
        return value;
    }
    
    function checkValue(uint amount) view public returns(bool) {
        return amount >= value;        
    }

    function loan() view public returns(bool) {
        return value > 0;
    }
}

contract MyFirstContract is Bank(10) {
    string private name;
    uint private age;
    
    function setName(string _name) public {
        name = _name;
    }
    
    function getName() view public returns (string) {
        return name;
    }
    
    function setAge(uint _age) public {
        age = _age;
    }
    
    function getAge() view public returns (uint) {
        return age;
    }
}
