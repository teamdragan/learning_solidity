pragma solidity ^0.4.0;

contract DataTypes {
    bool myBool = false;
    
    int8 myInt = -128;
    uint8 myUint = 255;
    
    string myString = "dragan";
    
    byte myByte = 0xFE;
    
    enum Action {ADD, REMOVE, UPDATE}
    Action myAction = Action.ADD;
    
    address myAddress = 0;
    function assignAddress() public {
        myAddress = msg.sender;
        myAddress.balance;
        myAddress.transfer(10);
    }
    
    // variable lenght array
    uint[] myIntArr = [1,2,3];
    function arrFunc() public {
        myIntArr.push(4);
        myIntArr.length;
        myIntArr[0];
    }
    
    // fixed length array
    uint[10] myFixedArray;
    
    struct Account {
        uint balance;
        uint dailyLimit;
    }
    Account myAccount;
    
    function structFunc() public {
        myAccount.balance = 100;
    }
    
    mapping (address => Account) accounts;
    
    function () payable public {
        accounts[msg.sender].balance += msg.value;
    }
    
    function getBalance() view public returns(uint) {
        return accounts[msg.sender].balance;
    }
}
