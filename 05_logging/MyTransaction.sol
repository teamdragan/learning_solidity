pragma solidity ^0.4.0;

/*
- Open in Remix
- Compile
- Run/Create
- Set value in Remix to 1 then click 'fallback'
  > it should succeed
- Set value in Remix to 0.1 then click 'fallback'
  > it should fail, because value is expected to be >= 1 ether
- Set value to 1, change address then click 'fallback'
  > it should fail, because only owner can do the transaction
*/
contract MyTransaction {
    event SenderLogger(address);
    event ValueLogger(uint);

    address private owner;
    
    modifier isOwner {
        require(owner == msg.sender);
        _;
    }
    
    modifier validValue {
        assert(msg.value >= 1 ether);
        _;
    }
    
    function MyTransaction() public {
        owner = msg.sender;
    }
    
    // fallback function
    function () payable isOwner validValue public {
        SenderLogger(msg.sender);
        ValueLogger(msg.value);
    }
}
