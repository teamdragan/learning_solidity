pragma solidity ^0.4.0;

import "./MyLibrary.sol";

contract TestMyLibrary {
    using IntExtended for uint;
    
    function testIncrement(uint _base) pure public returns(uint) {
        return _base.increment();
    }
    
    function testDecrement(uint _base) pure public returns(uint) {
        return _base.decrement();
    }
    
    function testIncrementByValue(uint _base, uint _value) pure public returns(uint) {
        return _base.incrementByValue(_value);
    }
    
    function testDecrementByValue(uint _base, uint _value) pure public returns(uint) {
        return _base.decrementByValue(_value);
    }
}
