pragma solidity ^0.4.0;

import "./ERC20.sol";

contract MyFirstToken is ERC20 {
    string public constant SYMBOL = "MFT";
    string public constant NAME = "My First Token";
    uint8 public constant DECIMALS = 18;
    
    uint private constant TOTAL_SUPPLY = 1000;
    mapping (address => uint) private __balanceOf;
    mapping (address => mapping (address => uint)) private __allowances;
    
    function MyFirstToken() public {
            __balanceOf[msg.sender] = TOTAL_SUPPLY;
    }
    
    function totalSupply() public view returns (uint _totalSupply) {
        _totalSupply = TOTAL_SUPPLY;
    }
    
    function balanceOf(address _addr) public view returns (uint balance) {
        return __balanceOf[_addr];
    }
    
    function transfer(
        address _to, 
        uint _value) public returns (bool success)
    {
        if (_value > 0 && _value <= balanceOf(msg.sender)) {
            __balanceOf[msg.sender] -= _value;
            __balanceOf[_to] += _value;
            return true;
        }
        return false;
    }
    
    function transferFrom(
        address _from,
        address _to,
        uint _value) public returns (bool success)
    {
        if (__allowances[_from][msg.sender] > 0 &&
            _value > 0 &&
            __allowances[_from][msg.sender] >= _value)
        {
            __balanceOf[_from] -= _value;
            __balanceOf[_to] += _value;
            // Missed from the video
            __allowances[_from][msg.sender] -= _value;
            return true;
        }
        return false;
    }
    
    function approve(
        address _spender,
        uint _value) public returns (bool success)
    {
        __allowances[msg.sender][_spender] = _value;
        return true;
    }
    
    function allowance(
        address _owner,
        address _spender) public view returns (uint remaining)
    {
        return __allowances[_owner][_spender];
    }
}
