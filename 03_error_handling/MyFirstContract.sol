pragma solidity ^0.4.0;

interface Regulator {
    function checkValue(uint amount) view public returns(bool);    
    function loan() view public returns(bool);
}

contract Bank is Regulator {
    uint private value;
    address private owner;

    modifier ownerFunc {
        require(owner == msg.sender);
        _;
    }

    function Bank(uint amount) public {
        value = amount;
        owner = msg.sender;
    }

    function deposit(uint amount) ownerFunc public {
        value += amount;
    }
    
    function withdraw(uint amount) ownerFunc public {
        if (checkValue(amount)) {
            value -= amount;
        }
    }
    
    function balance() view public returns (uint) {
        return value;
    }
    
    function checkValue(uint amount) view public returns(bool) {
        return value >= amount;        
    }

    function loan() view public returns(bool) {
        return value > 0;
    }
}

contract MyFirstContract is Bank(10) {
    string private name;
    uint private age;
    
    function setName(string _name) public {
        name = _name;
    }
    
    function getName() view public returns (string) {
        return name;
    }
    
    function setAge(uint _age) public {
        age = _age;
    }
    
    function getAge() view public returns (uint) {
        return age;
    }
}
